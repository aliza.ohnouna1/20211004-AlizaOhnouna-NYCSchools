This project displays a list of NYC high schools and relevant data including SAT scores, contact information, and a description. Users can save favorite high schools to a favorites tab.
